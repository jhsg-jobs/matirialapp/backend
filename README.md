# API de Materiais

Esta Api é um modelo de uma arvore de materiais, onde temos materiais que são considerados produtos finais e tambem temos os materiais "filhos" são sempre materias primas.

[EndPoint Principal](http://localhost:8080/api/v1/materiais)

# Instruções 

Está API foi desenvolvida com o banco H2 para desenvolvimento e o Postgress para teste e deploy em produção. 
Na pasta /src/main/resources/db/insomnia temos um arquivo Api-Endpoints.json no qual sendo importado no Insomnia auxilia a ver todos os endpoints disponíveis  

### Requisitos
* Java 11
* Maven
* Docker

### IDE
* Eclipse, Intellij, Vscode ou STS

### Imagem postgress
Para usar o postgress é necessário que a propriedade spring.profiles.active no aplication.properties estaja para test.

Iniciar imagem docker postgress 
* docker-compose -f "docker-compose.yml" up -d --build
Parar imagem docker postgress
* docker-compose -f "docker-compose.yml" down


### Links Uteis
* PgAdmin: http://localhost:4040/login?next=%2F
* DB H2: http://localhost:8080/api/h2-console
* Swagger: http://localhost:8080/swagger-ui.html


