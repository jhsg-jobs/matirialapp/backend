package br.com.jhsgdev.backend.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
//public class SwaggerConfig extends WebMvcConfigurationSupport {

    @Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.jhsgdev.backend"))
				.paths(PathSelectors.regex("/api/*"))
				.build();
	}

    private ApiInfo apiInfo() {
		return new ApiInfo(
				"RESTful API com Spring Boot", 
				"Api para cadastro de materiais.",
				"v1",
				"Terms Of Service Url",
				new Contact("Jefferson Henrique", "https://gitlab.com/jhsg-dev", "jhsg.dev@gmail.com"),
				"License of API", "License of URL", Collections.emptyList());
	}
    
//    @Override
//    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
//      registry.addResourceHandler("swagger-ui.html")
//          .addResourceLocations("classpath:/META-INF/resources/");
//
//      registry.addResourceHandler("/webjars/**")
//          .addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }
}