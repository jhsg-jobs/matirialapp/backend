package br.com.jhsgdev.backend.model.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.jhsgdev.backend.model.enums.StatusOrdemProducao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class OrdemProducaoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String codigo;
	private StatusOrdemProducao status;
	private Date data;

	@JsonIgnoreProperties({ "ordemProducao" })
	private MaterialDTO material;
}
