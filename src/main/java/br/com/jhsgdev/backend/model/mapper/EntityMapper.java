package br.com.jhsgdev.backend.model.mapper;

import java.util.List;

/**
 * interface generica Mapper 
 * 
 * @param <D>
 * @param <E>
 */
public interface EntityMapper<D, E> {
    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntity(List<D> dtoList);

    List<D> toDto(List<E> entityList);
}