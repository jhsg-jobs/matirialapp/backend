package br.com.jhsgdev.backend.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "tb_material", schema = "dbmateriais")
public class Material implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nome;
	private String descricao;
	@Column(name = "materia_prima")
	private Boolean materiaPrima;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "id_estoque", referencedColumnName = "id")
	private Estoque estoque;

	@OneToMany(mappedBy = "material", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<OrdemProducao> ordemProducao = new ArrayList<OrdemProducao>();

	@Transient
	Set<Material> materialParente = new HashSet<>();

	public Material(Long id, String nome, String descricao, Boolean materiaPrima) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.materiaPrima = materiaPrima;
	}

}