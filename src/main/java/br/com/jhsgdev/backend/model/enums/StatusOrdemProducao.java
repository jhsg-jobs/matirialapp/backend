package br.com.jhsgdev.backend.model.enums;

import lombok.Getter;

@Getter
public enum StatusOrdemProducao {

	PENDENTE(1, "Pendente"), APROVADA(2, "Aprovada"), REPROVADA(3, "Reprovada"), CANCELADA(4, "Cancelada");

	private Integer codigo;
	private String descricao;

	/**
	 * @param codigo
	 * @param descricao
	 */
	StatusOrdemProducao(Integer codigo, String descricao) {
		this.descricao = descricao;
		this.codigo = codigo;
	}

	/**
	 * @param idCampo
	 * @return
	 */
	public static StatusOrdemProducao getPorCodigo(Integer idCampo) {
		for (StatusOrdemProducao tipo : StatusOrdemProducao.values()) {
			if (tipo.codigo.equals(idCampo)) {
				return tipo;
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * @return
	 */
	public Integer getCodigo() {
		return this.codigo;
	}
}
