package br.com.jhsgdev.backend.model.mapper;

import org.mapstruct.Mapper;

import br.com.jhsgdev.backend.model.dto.MaterialDTO;
import br.com.jhsgdev.backend.model.entity.Material;




@Mapper(componentModel = "spring")
public interface MaterialMapper extends EntityMapper<MaterialDTO, Material> {


}
