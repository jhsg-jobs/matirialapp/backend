package br.com.jhsgdev.backend.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = { "material" })
@Table(name = "tb_estoque", schema = "dbmateriais")
public class Estoque implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long quantidade;

	@OneToOne(mappedBy = "estoque", fetch = FetchType.LAZY)
	private Material material;

	public Estoque(Long id, Long quantidade) {
		super();
		this.id = id;
		this.quantidade = quantidade;
	}

}
