package br.com.jhsgdev.backend.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MaterialDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String descricao;
	private Boolean materiaPrima;
	private EstoqueDTO estoque;
	private List<OrdemProducaoDTO> ordemProducao = new ArrayList<>();
	private Set<MaterialDTO> materialParente = new HashSet<>();

}