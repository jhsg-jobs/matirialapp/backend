package br.com.jhsgdev.backend.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class EstoqueDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long quantidade;

	@JsonIgnoreProperties({ "estoque" })
	private MaterialDTO material;

	public EstoqueDTO(Long id, Long quantidade) {
		super();
		this.id = id;
		this.quantidade = quantidade;
	}

}
