package br.com.jhsgdev.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.jhsgdev.backend.model.dto.MaterialDTO;
import br.com.jhsgdev.backend.model.dto.OrdemProducaoDTO;
import br.com.jhsgdev.backend.service.MaterialService;
import br.com.jhsgdev.backend.service.OrdemProducaoService;

/**
 * OrdemProducaoController
 * 
 */
@RestController
@RequestMapping("/v1/ordem-producao")
@CrossOrigin(origins = "*")
public class OrdemProducaoController {
	
	@Autowired
	private OrdemProducaoService service;
    
	@GetMapping
	public List<OrdemProducaoDTO> findAll() {		
		List<OrdemProducaoDTO> list = this.service.findAll();
		return list;
	}
    
    @PostMapping
	public OrdemProducaoDTO create(@RequestBody OrdemProducaoDTO objVo) {
    	
    	objVo = this.service.create(objVo);    	
		return objVo;
	}
}