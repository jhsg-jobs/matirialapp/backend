package br.com.jhsgdev.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.jhsgdev.backend.model.dto.MaterialDTO;
import br.com.jhsgdev.backend.service.MaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * MaterialController
 */
@RestController
@Api(value = "Api Rest Material")
@RequestMapping("/v1/materiais")
@CrossOrigin(origins = "*")
public class MaterialController {

	@Autowired
	private MaterialService service;

	@ApiOperation(value = "Retorna um Material com seus respectivos materiais filhos.")
	@GetMapping("/{id}")
	public MaterialDTO show(@PathVariable Long id) {
		return this.service.show(id);
	}

	@ApiOperation(value = "Retorna uma lista de Materiais de acordo com o filtro materiaPrima informado, caso não informado retorna todos.")
	@GetMapping
	public List<MaterialDTO> findAll(
			@RequestParam(value = "materiaPrima", defaultValue = "null") String isMateriaPrima) {
		
		List<MaterialDTO> list = this.service.findAll(isMateriaPrima);
		return list;
	}

//	@GetMapping(value = "/page")
//	public ResponseEntity<Page<Material>> findByPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
//			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
//			@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
//			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
//
//		Page<Material> list = this.service.findPage(page, linesPerPage, orderBy, direction);
//		return ResponseEntity.ok().body(list);
//	}

	@ApiOperation(value = "Cadastra um Material juntamente com o seu estoque e relaciona com os materiais filhos.")
	@PostMapping
	public MaterialDTO create(@RequestBody MaterialDTO objVo) {
		return this.service.create(objVo);
	}
	
	@ApiOperation(value = "Atualiza um Material juntamente com o seu estoque e altera o relacionamento com os materiais filhos.")
	@PutMapping("/{id}")
	public MaterialDTO update(@PathVariable Long id, @RequestBody MaterialDTO objVo) {
		return this.service.update(id, objVo);
	}

	@ApiOperation(value = "Remove um Material juntamente com o seu estoque e todo o relacionamento com os materiais filhos.")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> destroy(@PathVariable Long id) {
		this.service.destroy(id);
		return ResponseEntity.noContent().build();
	}

}