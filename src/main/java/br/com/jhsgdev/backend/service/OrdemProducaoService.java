package br.com.jhsgdev.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.exception.ObjectNotFoundException;
import br.com.jhsgdev.backend.model.dto.MaterialDTO;
import br.com.jhsgdev.backend.model.dto.OrdemProducaoDTO;
import br.com.jhsgdev.backend.model.entity.Estoque;
import br.com.jhsgdev.backend.model.entity.Material;
import br.com.jhsgdev.backend.model.entity.MaterialParente;
import br.com.jhsgdev.backend.model.entity.OrdemProducao;
import br.com.jhsgdev.backend.model.enums.StatusOrdemProducao;
import br.com.jhsgdev.backend.repository.OrdemProducaoRepository;
import br.com.jhsgdev.backend.utils.RandomString;

// import br.com.jhsgdev.backend.repository.OrdemRepository;

/**
 * 
 * OrdemProducaoService
 * 
 * @author Jefferson Henrique Sousa Galvão
 *
 */
@Service
@Transactional
public class OrdemProducaoService {

	@Autowired
	private OrdemProducaoRepository repository;

	@Autowired
	MaterialService materialService;
	
	private Boolean estoqueValido = false;
	List<Estoque> estoquesAtualido = new ArrayList<>();

	
	public List<OrdemProducaoDTO> findAll() {
		List<OrdemProducao> ordens = this.repository.findAll();

		List<OrdemProducaoDTO> ordensDTO = new ArrayList<>();
		ordensDTO.addAll(this.toDTO(ordens));
		return ordensDTO;
	}

	public OrdemProducaoDTO create(OrdemProducaoDTO objVo) {

		Material entity = this.materialService.findById(objVo.getMaterial().getId());
		
		this.consutarEstoque(entity);

		OrdemProducao ordem = this.toEntity(objVo);
		ordem.setId(null);

		RandomString hash = new RandomString(8);
		ordem.setCodigo(hash.nextString());
		ordem.setData(new Date());
		ordem.setStatus(StatusOrdemProducao.PENDENTE);
		ordem.setMaterial(entity);

		ordem = this.repository.saveAndFlush(ordem);

		objVo = this.toDTO(ordem);
		objVo.setMaterial(this.materialService.toDTO(entity));

		return objVo;
	}
	
	public void consutarEstoque(Material material) throws ObjectNotFoundException{
		
		Material clone = new Material();
		clone = material;
		clone.getEstoque().setMaterial(null);

		List<Material> list = new ArrayList<Material>();
		list.add(clone);

		Set<Material> result = this.materialService.setParentes(list);
		List<Material> entity = new ArrayList<Material>(result);

		material = entity.get(0);
		material.getMaterialParente().clear();
		material.getMaterialParente().addAll(result);
		
		this.verificaEstoque(material);
		
	}
	
	
	protected Boolean verificaEstoque(Material material) throws ObjectNotFoundException{
		
		
		Set<Material> parentes = new HashSet<>();
		parentes = material.getMaterialParente();
		
		Estoque materialOrdem = material.getEstoque();
			

		parentes.forEach(m -> {
			m = this.verificaEstoqueParentes(m);
		});
		
		
		if(this.estoqueValido) {			
			materialOrdem.setQuantidade(materialOrdem.getQuantidade() + 1l);
			this.estoquesAtualido.add(materialOrdem);
			this.estoqueValido = true;
		}else {
			this.mensagemError("O Material " + material.getNome() + "está sem estoque!");
		}

		// falta salvar estoquesAtualido

		return this.estoqueValido;
	}

	private Material verificaEstoqueParentes(Material entity) throws ObjectNotFoundException{
		List<Material> list = new ArrayList<>(entity.getMaterialParente());	
		
		if (list != null) {
			for (Material f : list) {
				
				if(f.getEstoque().getQuantidade() > 0L) {
					f.getEstoque().setQuantidade(f.getEstoque().getQuantidade() - 1L);
					this.estoquesAtualido.add(f.getEstoque());
					this.estoqueValido = true;					
				}else {
					this.mensagemError("O Material " + f.getNome() + " está sem estoque!");
					this.estoqueValido = false;
				}
				if(!f.getMaterialParente().isEmpty() || f.getMaterialParente() != null) {
					f.getMaterialParente().forEach(p ->{						
						this.verificaEstoqueParentes(p);
					});
				}
			}

		}
		return entity;
	}
	
	public void mensagemError(String mensagem) throws ObjectNotFoundException{
		if(true) {
			throw new ObjectNotFoundException(mensagem);
		}		
	}
	

	public OrdemProducao toEntity(OrdemProducaoDTO objVO) {
		OrdemProducao obj = new OrdemProducao();

		if (objVO != null) {
			obj.setId(objVO.getId());
			obj.setCodigo(objVO.getCodigo());
			obj.setData(objVO.getData());
			obj.setStatus(objVO.getStatus());
		}

		return obj;
	}

	public List<OrdemProducao> toEntity(List<OrdemProducaoDTO> objVO) {
		List<OrdemProducao> list = new ArrayList<>();
		objVO.forEach(e -> {
			list.add(this.toEntity(e));
		});
		return list;
	}

	public OrdemProducaoDTO toDTO(OrdemProducao obj) {
		OrdemProducaoDTO objVO = new OrdemProducaoDTO();

		if (obj != null) {
			objVO.setId(obj.getId());
			objVO.setCodigo(obj.getCodigo());
			objVO.setData(obj.getData());
			objVO.setStatus(obj.getStatus());
			if (obj.getMaterial() != null)
				objVO.setMaterial(this.materialService.toDTO(obj.getMaterial()));
		}

		return objVO;
	}

	public List<OrdemProducaoDTO> toDTO(List<OrdemProducao> obj) {
		List<OrdemProducaoDTO> list = new ArrayList<>();
		obj.forEach(e -> {
			list.add(this.toDTO(e));
		});

		return list;
	}
	
	public Boolean getEstoqueValido() {
		return estoqueValido;
	}

	public void setEstoqueValido(Boolean estoqueValido) {
		this.estoqueValido = estoqueValido;
	}

	public List<Estoque> getEstoquesAtualido() {
		return estoquesAtualido;
	}

	public void setEstoquesAtualido(List<Estoque> estoquesAtualido) {
		this.estoquesAtualido = estoquesAtualido;
	}

}