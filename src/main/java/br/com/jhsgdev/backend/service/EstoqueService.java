package br.com.jhsgdev.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.exception.ObjectNotFoundException;
import br.com.jhsgdev.backend.model.dto.EstoqueDTO;
import br.com.jhsgdev.backend.model.entity.Estoque;
import br.com.jhsgdev.backend.model.entity.Material;
import br.com.jhsgdev.backend.repository.EstoqueRepository;

/**
 * 
 * EstoqueService
 * 
 * @author Jefferson Henrique Sousa Galvão
 *
 */
@Service
@Transactional
public class EstoqueService {

	@Autowired
	MaterialService materialService;

	@Autowired
	EstoqueRepository repository;

	public Estoque findById(Long id) {
		Optional<Estoque> entity = this.repository.findById(id);
		return entity
				.orElseThrow(() -> new ObjectNotFoundException("O estoque de id: " + id + ", não foi encontrado!"));
	}

	public Estoque findByMaterial(Material material) {
		Optional<Estoque> entity = this.repository.findByMaterial(material);
		return entity.get();
	}

	public Page<Estoque> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return this.repository.findAll(pageRequest);
	}

	public List<EstoqueDTO> findAll() {
		List<Estoque> result = this.repository.findAll();

		List<EstoqueDTO> response = this.toDTO(result);
		return response;
	}

	public EstoqueDTO create(EstoqueDTO objVo) {
		objVo.setId(null);

		Estoque entity = this.toEntity(objVo);
		entity.setMaterial(this.materialService.findId(objVo.getMaterial().getId()));

		Estoque result = this.save(entity);

		return this.toDTO(result);
	}

	public EstoqueDTO update(Long id, EstoqueDTO objVo) {
		Estoque entity = this.findById(id);
		objVo.setId(entity.getId());

		Estoque result = this.save(this.toEntity(objVo));

		return this.toDTO(result);
	}

	private Estoque save(Estoque entity) {

		return this.repository.saveAndFlush(entity);
	}

	public void destroy(Long id) {
		Estoque entity = this.findById(id);
		this.repository.delete(entity);
	}

	public Estoque toEntity(EstoqueDTO objVO) {
		Estoque obj = null;

		if (objVO != null)
			obj = new Estoque(objVO.getId(), objVO.getQuantidade());

		return obj;
	}

	public List<Estoque> toEntity(List<EstoqueDTO> objVO) {
		List<Estoque> list = new ArrayList<>();
		objVO.forEach(e -> {
			list.add(this.toEntity(e));
		});

		return list;
	}

	public EstoqueDTO toDTO(Estoque obj) {
		EstoqueDTO objVO = new EstoqueDTO();

		if (obj != null)
			objVO = new EstoqueDTO(obj.getId(), obj.getQuantidade());

		return objVO;
	}

	public List<EstoqueDTO> toDTO(List<Estoque> obj) {
		List<EstoqueDTO> list = new ArrayList<>();
		obj.forEach(e -> {
			list.add(this.toDTO(e));
		});

		return list;
	}

}