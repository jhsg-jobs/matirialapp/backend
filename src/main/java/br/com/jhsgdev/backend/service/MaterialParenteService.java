package br.com.jhsgdev.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.model.entity.Material;
import br.com.jhsgdev.backend.model.entity.MaterialParente;
import br.com.jhsgdev.backend.repository.MaterialParenteRepository;

/**
 * 
 * MaterialParenteService
 * 
 * @author Jefferson Henrique Sousa Galvão
 *
 */
@Service
@Transactional
public class MaterialParenteService {

	@Autowired
	MaterialParenteRepository repository;

	@Autowired
	MaterialService materialService;

	public MaterialParente findById(Long id) {
		Optional<MaterialParente> entity = this.repository.findById(id);
		return entity.get();
	}

	public Material create(Material obj, List<Long> idFilhos) {

		List<Material> filhos = new ArrayList<>();

		filhos = this.materialService.findByListId(idFilhos);

		obj.getMaterialParente().clear();

		List<MaterialParente> parentes = new ArrayList<>();

		filhos.forEach(p -> {
			obj.getMaterialParente().add(p);
			MaterialParente relacionamento = new MaterialParente(null, obj, p, 1L);

			parentes.add(relacionamento);
		});

		repository.saveAll(parentes);
		obj.getMaterialParente().addAll(filhos);

		return obj;
	}

}
