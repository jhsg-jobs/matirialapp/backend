package br.com.jhsgdev.backend.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.exception.ObjectNotFoundException;
import br.com.jhsgdev.backend.model.dto.MaterialDTO;
import br.com.jhsgdev.backend.model.entity.Estoque;
import br.com.jhsgdev.backend.model.entity.Material;
import br.com.jhsgdev.backend.model.entity.MaterialParente;
import br.com.jhsgdev.backend.repository.MaterialParenteRepository;
import br.com.jhsgdev.backend.repository.MaterialRepository;

/**
 * 
 * MaterialService
 * 
 * @author Jefferson Henrique Sousa Galvão
 *
 */
@Service
@Transactional
public class MaterialService {

	@Autowired
	MaterialRepository repository;

	@Autowired
	MaterialParenteRepository materialParenteRepository;

	@Autowired
	EstoqueService estoqueService;

	@Autowired
	MaterialParenteService materialParenteService;

	public Material findById(Long id) {
		Optional<Material> entity = this.repository.findById(id);
		return entity
				.orElseThrow(() -> new ObjectNotFoundException("O Material de Id: " + id + ", não foi encontrado!"));
	}

	public Material findId(Long id) {
		Optional<Material> entity = this.repository.findById(id);
		return entity.get();
	}

	public List<Material> findByListId(List<Long> ids) {
		List<Material> list = this.repository.findByListId(ids);
		return list;
	}

	public Page<Material> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return this.repository.findAll(pageRequest);
	}

	public List<MaterialDTO> findAll(String isMateriaPrima) {

		List<Material> material = new ArrayList<>();

		if (isMateriaPrima.equals("null")) {
			material = this.repository.findAll();
		} else {
			boolean materiaPrima = Boolean.parseBoolean(isMateriaPrima);

			material = this.repository.findByMateriaPrima(materiaPrima);
		}

		Set<Material> result = this.setParentes(material);
		Set<MaterialDTO> response = this.toDTO(result);

		List<MaterialDTO> list = new ArrayList<MaterialDTO>(response);
		return list;
	}

	public MaterialDTO show(Long id) {
		Material obj = this.findById(id);

		Material clone = new Material();
		clone = obj;
		clone.getEstoque().setMaterial(null);

		List<Material> list = new ArrayList<Material>();
		list.add(clone);

		Set<Material> result = this.setParentes(list);
		List<Material> entity = new ArrayList<Material>(result);

		obj = entity.get(0);

		MaterialDTO dto = this.toDTO(obj);
		dto.setEstoque(this.estoqueService.toDTO(obj.getEstoque()));

		return dto;
	}

	private List<Long> getIdFilhos(MaterialDTO objVo) {
		List<Long> idFilhos = new ArrayList<>();

		objVo.getMaterialParente().stream().forEach(p -> {
			idFilhos.add(p.getId());
		});
		return idFilhos;
	}

	public MaterialDTO create(MaterialDTO objVo) {
		objVo.setId(null);
		List<Long> idFilhos = this.getIdFilhos(objVo);

		Material entity = this.toEntity(objVo);
		Estoque estoque = this.estoqueService.toEntity(objVo.getEstoque());
		estoque.setMaterial(entity);
		entity.setEstoque(estoque);

		entity = this.save(entity);

		entity = this.materialParenteService.create(entity, idFilhos);

		MaterialDTO response = this.toDTO(entity);
		return response;
	}

	public MaterialDTO update(Long id, MaterialDTO objVo) {
		Material entity = this.findById(id);

		objVo.setId(entity.getId());

		List<Long> idFilhos = this.getIdFilhos(objVo);

		entity.setNome(objVo.getNome());
		entity.setDescricao(objVo.getDescricao());
		entity.getEstoque().setQuantidade(objVo.getEstoque().getQuantidade());

		entity = this.save(entity);

		this.materialParenteRepository.deleteByMaterial(entity);
		entity = this.materialParenteService.create(entity, idFilhos);

		MaterialDTO response = this.toDTO(entity);
		return response;
	}

	private Material save(Material entity) {
		if (entity.getNome() == null || entity.getNome() == "") {
			throw new RuntimeException("Favor informar o nome do Material!");
		}
		return this.repository.saveAndFlush(entity);
	}

	public void destroy(Long id) {
        Material entity = this.findById(id);
        this.materialParenteRepository.deleteByMaterial(entity);
		this.repository.delete(entity);
	}

	protected Set<Material> setParentes(List<Material> list) {
		List<Material> l = new ArrayList<>();
		l = list;

		l.forEach(m -> {
			// System.out.println("Pai: "+ m.getId());
			m = this.buscarParentes(m);

		});

		Set<Material> clone = new HashSet<>(l);

		return clone;
	}

	private Material buscarParentes(Material entity) {
		List<MaterialParente> list = new ArrayList<>();
		list = materialParenteRepository.findByMaterial(entity);
		if (list != null) {
			for (MaterialParente f : list) {
				// entity.setQuantidadeMaterialProducao(f.getQuantidade());
				entity.getMaterialParente().add(f.getMaterialParente());
				this.buscarParentes(f.getMaterialParente());
			}

		}
		return entity;
	}

	public Material toEntity(MaterialDTO objVO) {
		Material obj = new Material();

		if (objVO != null) {

			obj.setId(objVO.getId());
			obj.setNome(objVO.getNome());
			obj.setDescricao(objVO.getDescricao());
			obj.setMateriaPrima(objVO.getMateriaPrima());

			if (objVO.getMaterialParente() != null) {
				objVO.getMaterialParente().forEach(m -> {
					obj.getMaterialParente().add(this.toEntity(m));
				});
			}
		}

		return obj;
	}

	public Set<Material> toEntity(Set<MaterialDTO> objVO) {
		Set<Material> list = new HashSet<>();
		objVO.forEach(e -> {
			list.add(this.toEntity(e));
		});

		return list;
	}

	public MaterialDTO toDTO(Material obj) {
		MaterialDTO objVO = new MaterialDTO();

		if (objVO != null) {
			objVO.setId(obj.getId());
			objVO.setNome(obj.getNome());
			objVO.setDescricao(obj.getDescricao());
			objVO.setMateriaPrima(obj.getMateriaPrima());
			objVO.setEstoque(this.estoqueService.toDTO(obj.getEstoque()));

			if (obj.getMaterialParente() != null) {
				obj.getMaterialParente().forEach(m -> {
					objVO.getMaterialParente().add(this.toDTO(m));
				});
			}
		}

		return objVO;
	}

	public Set<MaterialDTO> toDTO(Set<Material> obj) {
		Set<MaterialDTO> list = new HashSet<>();
		obj.forEach(e -> {
			list.add(this.toDTO(e));
		});

		return list;
	}

}