package br.com.jhsgdev.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.model.entity.Estoque;
import br.com.jhsgdev.backend.model.entity.Material;

public interface EstoqueRepository extends JpaRepository<Estoque, Long> {

	@Transactional(readOnly = true)
	Optional<Estoque> findByMaterial(Material entity);

}