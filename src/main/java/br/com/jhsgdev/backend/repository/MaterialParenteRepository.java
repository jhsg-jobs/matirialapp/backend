package br.com.jhsgdev.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.model.entity.Material;
import br.com.jhsgdev.backend.model.entity.MaterialParente;

public interface MaterialParenteRepository extends JpaRepository<MaterialParente, Long> {

	@Transactional(readOnly = true)
	List<MaterialParente> findByMaterial(Material entity);

	@Modifying
	@Query("delete from MaterialParente m where m.material = :entity")
	void deleteByMaterial(@Param("entity") Material entity);

}