package br.com.jhsgdev.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.jhsgdev.backend.model.entity.OrdemProducao;

public interface OrdemProducaoRepository extends JpaRepository<OrdemProducao, Long> {
    
}