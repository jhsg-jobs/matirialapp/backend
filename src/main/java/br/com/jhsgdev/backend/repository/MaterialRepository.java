package br.com.jhsgdev.backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import br.com.jhsgdev.backend.model.entity.Estoque;
import br.com.jhsgdev.backend.model.entity.Material;

public interface MaterialRepository extends JpaRepository<Material, Long> {

	@Transactional(readOnly = true)
	Optional<Material> findByEstoque(Estoque entity);

	@Transactional(readOnly = true)
	@Query("SELECT m FROM Material m WHERE m.id IN (:ids)")
	List<Material> findByListId(List<Long> ids);

	@Transactional(readOnly = true)
	List<Material> findByMateriaPrima(Boolean materiaPrima);

}